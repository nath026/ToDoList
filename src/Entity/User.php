<?php

namespace App\Entity;

use Carbon\Carbon;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(type="datetime")
     */
    private $birthday;

    /**
     * @ORM\OneToOne(targetEntity=ToDoList::class, mappedBy="User", cascade={"persist", "remove"})
     */
    private $toDoList;

    public function isValid()
    {
        if (empty($this->email)) {
            throw new Exception("L'adresse mail est vide");
        } elseif (filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            throw new Exception("Adresse mail invalide");
        }

        if (empty($this->lastName)) {
            throw new Exception("Le nom de famille est vide");
        }

        if (empty($this->firstName)) {
            throw new Exception("Le prénom est vide");
        }

        if ((($this->birthday - (Carbon::now()->year)) < 13)
            || ($this->birthday - (Carbon::now()->year)) > 125
        ) {
            throw new Exception("Age invalide, l'utilisateur doit avoir plus de 13 ans et moins de 125 ans");
        }

        if (empty($this->password)) {
            throw new Exception('Le mot de passe est vide');
        } elseif (strlen($this->password) < 8 || strlen($this->password) > 40) {
            throw new Exception('Le mot de passe doit faire entre 8 et 40 caractères');
        }

        return true;
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getToDoList(): ?ToDoList
    {
        return $this->toDoList;
    }

    public function setToDoList(?ToDoList $toDoList): self
    {
        // unset the owning side of the relation if necessary
        if ($toDoList === null && $this->toDoList !== null) {
            $this->toDoList->setUser(null);
        }

        // set the owning side of the relation if necessary
        if ($toDoList !== null && $toDoList->getUser() !== $this) {
            $toDoList->setUser($this);
        }

        $this->toDoList = $toDoList;

        return $this;
    }
}
