<?php

namespace App\Entity;

use App\Repository\ToDoListRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Carbon\Carbon;

/**
 * @ORM\Entity(repositoryClass=ToDoListRepository::class)
 */
class ToDoList
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="toDoLis", cascade={"persist", "remove"})
     */
    private $User;

    /**
     * @ORM\OneToMany(targetEntity=Item::class, mappedBy="toDoList")
     */
    private $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function getLastItem(): ?Item
    {
        return $this->getItems()->last();
    }

    public function getSizeToDoList()
    {
        return sizeof($this->getItems());
    }

    public function eighthItem()
    {
        if ($this->getSizeToDoList() == 8) {
            return ("Vous avez 8 objets dans la liste, il ne reste plus que 2 places libre");
        }
    }

    public function addItem(Item $item): self
    {
        if ($this->getSizeToDoList() >= 10) {
            return new Exception("La liste est pleine, ne peut contenir que 10 objets maximum");
        }
        if (is_null($item) || !$item->isValid()) {
            throw new Exception("Cet objet est invalide");
        }
        if ($this->items->contains($item->getName())) {
            throw new Exception("Cet objet existe déjà");
        }
        $lastItem = $this->getLastItem();
        if (!is_null($this->getLastItem()) && Carbon::now()->subMinutes(30)->isBefore($lastItem->createdAt)) {
            throw new Exception("Il faut attendre 30 minutes avant de créer un nouvel objet");
        }

        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setToDoList($this);
        }
        $this->eighthItem();
        return $this;


        return $this;
    }

    public function removeItem(Item $item): self
    {
        if ($this->items->removeElement($item)) {
            // set the owning side to null (unless already changed)
            if ($item->getToDoList() === $this) {
                $item->setToDoList(null);
            }
        }

        return $this;
    }
}
