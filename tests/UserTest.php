<?php

namespace App\Tests;

use App\Entity\User;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;


class UserTest extends TestCase
{

    private User $user;

    protected function setUp(): void
    {
        $this->user = new User('test@test.fr', 'password123', 'Prenom', 'Nom', Carbon::now()->subDecades(2));
        parent::setUp();
    }

    public function testIsValidNominal()
    {
        $this->assertTrue($this->user->isValid());
    }

    public function testIsNotValidDueToEmptyFirstname()
    {
        $this->user->setFirstName('');
        $this->assertFalse($this->user->isValid());
    }

    public function testIsNotValidDueToEmptyLastname()
    {
        $this->user->setLastName('');
        $this->assertFalse($this->user->isValid());
    }

    public function testIsNotValidDueToEmptyEmail()
    {
        $this->user->setEmail('');
        $this->assertFalse($this->user->isValid());
    }

    public function testIsNotValidDueToBadEmail()
    {
        $this->user->setEmail('pasbon');
        $this->assertFalse($this->user->isValid());
    }

    public function testIsNotValidDueToEmptyPassword()
    {
        $this->user->setEmail('');
        $this->assertFalse($this->user->isValid());
    }

    public function testIsNotValidDueToShortPassword()
    {
        $this->user->setEmail('123');
        $this->assertFalse($this->user->isValid());
    }
    public function testIsNotValidDueToLongPassword()
    {
        $this->user->setEmail('azertyuiopqsdfghklmwxcvbnazertyuiopqsdfghjklmwxcvbn');
        $this->assertFalse($this->user->isValid());
    }

    public function testIsNotValidDueToBirthdayToYoung()
    {
        $this->user->setBirthday(Carbon::now()->subYears(10));
        $this->assertFalse($this->user->isValid());
    }

    public function testIsNotValidDueToBirthdayTooOld()
    {
        $this->user->setBirthday(Carbon::now()->subDecades(20));
        $this->assertFalse($this->user->isValid());
    }

    public function testIsNotValidDueToBirthdayInFuture()
    {
        $this->user->setBirthday(Carbon::now()->addDecade());
        $this->assertFalse($this->user->isValid());
    }

    public function testIsValideDueToEmail()
    {
        $this->user->setEmail('email@test.test');
        $this->assertTrue($this->user->isValid());
    }

    public function testIsValideDueToFirstName()
    {
        $this->user->setFirstName('Helene');
        $this->assertTrue($this->user->isValid());
    }

    public function testIsValideDueToLastName()
    {
        $this->user->setFirstName('Dupont');
        $this->assertTrue($this->user->isValid());
    }
    public function testIsValideDueToBirthday()
    {
        $this->user->setBirthday(Carbon::now()->subDecades(3));
        $this->assertTrue($this->user->isValid());
    }
    public function testIsValidDueToPassword()
    {
        $this->user->setPassword('1234567890');
        $this->assertTrue($this->user->isValid());
    }
}
