<?php

namespace App\Tests;

use App\Entity\Item;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class ItemTest extends TestCase
{

    private $item;

    protected function setUp(): void
    {
        parent::setUp();


        $this->item = new Item(
            'name',
            'content description',
            Carbon::now()->subDays(14)->subMonth(7),
        );
    }
    public function testIsValidNominal()
    {
        $this->assertTrue($this->item->isValid());
    }

    public function testIsNotValideDueToEmptyName()
    {
        $this->item->setName('');
        $this->assertFalse($this->item->isValid());
    }

    public function testIsNotValideDueToEmptyContent()
    {
        $this->item->setContent('');
        $this->assertFalse($this->item->isValid());
    }

    public function testIsNotValidDueToLongContent()
    {
        $this->item->setContent("
            Les textes de 1000 mots et moins
            Vous avez peut-être remarqué, mais on est une gang d'écrivains ces temps-ci à travailler sur des textes de 750 mots. C'est une commande. Bientôt, on pourra vous dire de qui. : Moi en tout cas j'ai hâte de lire le produit final!
            J'adore concevoir des textes d'environ 1000 mots! :
            Mais c'est pas facile. Parce qu'en moins de 1000 mots, y'a pas de place pour les maladresses, les erreurs, les redites, les explications complexes. Faut bien peser chaque mot, chaque phrase, sélectionner les plus évocateurs...
            En même temps, cette longueur présente plusieurs avantages. J'ai souvent l'impression que c'est le domaine de l'idée solitaire.
            Par exemple, vous avez une idée de style d'écriture différent? C'est le moment de l'expérimenter. Avec un texte aussi court, même si c'est difficile de bien utiliser ce style nouveau, vous pouvez retravailler plusieurs fois sans que ça bouffe trop de temps. Et même si ce que vous racontez est un peu banal, peu importe, le style prendra toute la place.
            À l'opposé, vous avez une idée pour une anecdote, une technologie bizarre, un personnage inusité, etc., mais pas d'un roman où intégrer cette idée? C'est le moment de la mettre en scène. Il faut alors se centrer sur un moment de la vie des personnages ou de l'existence de cet univers. On sait ce qui arrive avant, peut-être aussi ce qui se passera après, et on l'évoque en quelques mots, mais c'est l'anecdote, la technologie, le personnage, etc., qui prend toute la place.
            Personnellement, sur 1000 mots et moins, je privilégie soit un texte très dialogué, soit un texte avec une narration distante, qui permet de couvrir une plus longue période de temps. Mais il y a moyen de mélanger les deux.
            Et la beauté des textes de 1000 mots et moins, c'est qu'une fois qu'on a fini, il ne reste plus... qu'à couper!
            Parce que, bonyenne, on arrive toujours trop long le premier coup! :p
            La commande de 750 mots en faisait 1000 hier. N'en reste que 880 ce matin. Où sont mes ciseaux?
            ");
        $this->assertFalse($this->item->isValid());
    }

    public function testIsNotValideDueToEmptyCreatedAt()
    {
        $this->item->setCreatedAt('');
        $this->assertFalse($this->item->isValid());
    }
    public function testIsNotValideDueToFuturCreatedAt()
    {
        $this->item->setCreatedAt(Carbon::now()->addDecade());
        $this->assertFalse($this->item->isValid());
    }

    public function testIstValideDueToName()
    {
        $this->item->setName('test');
        $this->assertTrue($this->item->isValid());
    }

    public function testIsNotValideDueToContent()
    {
        $this->item->setContent('description');
        $this->assertTrue($this->item->isValid());
    }

    public function testIsNotValideDueToCreatedAt()
    {
        $this->item->setCreatedAt(Carbon::now()->subDays(10));
        $this->assertTrue($this->item->isValid());
    }
}
