<?php

namespace App\Tests;

use App\Entity\User;
use App\Entity\Item;
use App\Entity\Todolist;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;


class ToDoListTest extends TestCase
{

    private User $user;
    private $item;
    private $Todolist;


    protected function setUp(): void
    {
        parent::setUp();


        $this->item = new Item(
            'name',
            'content description',
            Carbon::now()->subDays(14)->subMonth(7),
        );

        $this->user = new User(
            'test@test.fr',
            'password123',
            'Prenom',
            'Nom',
            Carbon::now()->subDecades(2)
        );


        $this->Todolist = $this->getMockBuilder(Todolist::class)
            ->onlyMethods(['addItem', 'getSizeToDoList', 'getLastItem'])
            ->getMock();
        $this->Todolist->user = $this->user;
        $this->Todolist->expects($this->any())->method('getLastItem')->willReturn($this->item);
    }

    public function testAddItemNominal()
    {
        $this->Todolist->expects($this->any())->method('getSizeToDoList')->willReturn(1);


        $addedItem = $this->Todolist->addItem($this->item);

        $this->assertNotNull($addedItem);
        $this->assertEquals('name', $addedItem->getName());
    }

    public function testToDoListMax()
    {
        $this->Todolist->expects($this->any())->method('getSizeToDoList')->willReturn(10);

        $this->expectException('Exception');
        $this->expectExceptionMessage('La liste est pleine, ne peut contenir que 10 objets maximum');

        $addedItem = $this->Todolist->addItem($this->item);
        $this->assertTrue($addedItem);
    }
}
